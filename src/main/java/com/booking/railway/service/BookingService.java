package com.booking.railway.service;

import com.booking.railway.domain.Booking;
import com.booking.railway.domain.Coach;
import com.booking.railway.domain.Passenger;
import com.booking.railway.enums.*;
import com.booking.railway.repository.BookingRepository;
import com.booking.railway.repository.CoachRepository;
import com.booking.railway.repository.PassengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class BookingService {

    @Autowired
    BookingRepository bookingRepository;
    @Autowired
    PassengerRepository passengerRepository;
    @Autowired
    CoachRepository coachRepository;

    private SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmm");
    protected static final EnumMap<Berth, List<Integer>> normalSeats = new EnumMap<>(Berth.class);
    protected static final EnumMap<Berth, List<Integer>> racSeats = new EnumMap<>(Berth.class);

    static {
        normalSeats.put(Berth.LOWER, Arrays.asList(1, 4));
        normalSeats.put(Berth.MIDDLE, Arrays.asList(2, 5));
        normalSeats.put(Berth.UPPER, Arrays.asList(3, 6));
        racSeats.put(Berth.SIDE_LOWER, Arrays.asList(7));
        racSeats.put(Berth.SIDE_UPPER, Arrays.asList(8));
    }

    public Booking newTicket(Passenger passenger) {
        Booking booking = new Booking();
        if (bookingRepository.countByStatus(BookingStatus.CONFIRMED) < 24) {
            booking = initBooking(passenger, BookingStatus.CONFIRMED, false);
        } else {
            boolean isLadies = passenger.getGender() == Gender.FEMALE;
            booking = initBooking(passenger, BookingStatus.WAITING_LIST, isLadies);
        }
        return booking;
    }

    public Booking initBooking(Passenger passenger, BookingStatus status, boolean isLadiesCheck) {
        Booking booking = new Booking();
        booking.setDate(new Date());
        booking.setCnfNo(format.format(booking.getDate()));
        booking.setPassenger(passenger);
        Coach coach = new Coach();
        coach.setStatus(CoachStatus.OPEN);
        Set<Berth> checkedBerths = berthsToAllow(passenger, status);
        coach = processSeatsByBerth(coach, checkedBerths, isLadiesCheck);

        if (coach.getStatus() == CoachStatus.OPEN) {
            Set<Berth> berths = (status == BookingStatus.WAITING_LIST) ? racSeats.keySet() : normalSeats.keySet();
            berths.removeAll(checkedBerths);
            coach = processSeatsByBerth(coach, berths, isLadiesCheck);
        }
        booking.setCoach(coach);
        booking.setStatus(status);

        if (coach.getStatus() == CoachStatus.OPEN) {
            booking.setOccupiedByMen(true);
            return booking;
        }

        booking = bookingRepository.save(booking);
        return booking;
    }

    public Set<Berth> berthsToAllow(Passenger passenger, BookingStatus status) {
        Set<Berth> checkedBerths = new HashSet<>();

        if (passenger.getAge() >= 60 ||
            (passenger.getGender() == Gender.FEMALE && passenger.getChildren() != null && !passenger.getChildren().isEmpty())) {
            if (status == BookingStatus.WAITING_LIST)
                checkedBerths.add(Berth.SIDE_LOWER);
            else
                checkedBerths.add(Berth.LOWER);
        } else {
            if (status == BookingStatus.WAITING_LIST)
                checkedBerths.add(Berth.SIDE_UPPER);
            else {
                checkedBerths.add(Berth.UPPER);
                checkedBerths.add(Berth.MIDDLE);
            }
        }

        return checkedBerths;
    }

    private Coach processSeatsByBerth(Coach coach, Collection<Berth> berths, boolean isLadies) {
        for (CoachNumber coachNumber : CoachNumber.values()) {
            if (isLadies) {
                Set<Gender> genders = bookingRepository.findByCoachCoachNumber(coachNumber);
                if (genders.contains(Gender.FEMALE) || genders.contains(Gender.OTHER)) {
                    coach = finalizeBooking(coach, berths, coachNumber);
                }
            } else {
                coach = finalizeBooking(coach, berths, coachNumber);
            }

            if (coach.getStatus() == CoachStatus.BOOKED)
                break;
        }
        return coach;
    }

    private Coach finalizeBooking(Coach coach, Collection<Berth> berths, CoachNumber coachNumber) {
        for (Berth berth : berths) {
            List<Integer> seats = getSeatsByBerth(berth);
            if (seats != null) {
                for (Integer seatNo : getSeatsByBerth(berth)) {
                    boolean isOccupied = coachRepository.existsByCoachNumberAndBerthAndSeatNo(coachNumber, berth, seatNo);
                    if (!isOccupied) {
                        coach.setSeatNo(seatNo);
                        coach.setCoachNumber(coachNumber);
                        coach.setBerth(berth);
                        coach.setStatus(CoachStatus.BOOKED);
                        break;
                    }
                }
            }
        }
        return coach;
    }

    private List<Integer> getSeatsByBerth(Berth berth) {
        if (normalSeats.containsKey(berth))
            return normalSeats.get(berth);

        if (racSeats.containsKey(berth))
            return racSeats.get(berth);

        return null;
    }
}
