package com.booking.railway.resource;

import com.booking.railway.api.ApiResult;
import com.booking.railway.domain.Booking;
import com.booking.railway.domain.Passenger;
import com.booking.railway.domain.dto.Ticket;
import com.booking.railway.enums.Berth;
import com.booking.railway.enums.BookingStatus;
import com.booking.railway.repository.BookingRepository;
import com.booking.railway.repository.PassengerRepository;
import com.booking.railway.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/railway/booking")
public class BookingController {

    @Autowired
    private PassengerRepository passengerRepository;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private BookingRepository bookingRepository;

    @GetMapping
    public List<Passenger> getPassengers() {
        return passengerRepository.findAll();
    }

    @GetMapping("/get-tickets-by-berth/{berth}")
    public ApiResult<List<Ticket>> getTicketsByBerth(@PathVariable("berth") Berth berth) {
        List<Ticket> tickets = bookingRepository.findByCoachBerth(berth).stream().map(this::mapper).collect(Collectors.toList());

        return new ApiResult<>(true, tickets, "");
    }

    @PostMapping("/booking")
    public ApiResult<Ticket> bookTicket(@RequestBody Passenger passenger) {
        if (StringUtils.isEmpty(passenger.getUsername()))
            return new ApiResult<>(false, "Empty username");

        if (passenger.getChildren() != null) {
            if (passenger.getChildren().stream().filter(c -> c.getAge() > 5).count() > 0)
                return new ApiResult<>(false, "Above age 6 should be new booking");
        }
        if (passengerRepository.existsByUsername(passenger.getUsername()))
            return new ApiResult<>(false, "Username already exists");

        if (bookingRepository.countByStatus(BookingStatus.CONFIRMED) == 32 || bookingRepository.countByStatus(BookingStatus.WAITING_LIST) == 5)
            return new ApiResult<>(false, "No tickets available");

        Booking booking = bookingService.newTicket(passenger);

        if (booking.isOccupiedByMen())
            return new ApiResult<>(false, "No ticket is available for Ladies");

        return new ApiResult<>(true, mapper(booking), "Your ticket is in " + booking.getStatus() + " status");
    }

    private Ticket mapper(Booking booking) {
        Ticket ticket = new Ticket();
        ticket.setCnfNo(booking.getCnfNo());
        ticket.setCoachNumber(booking.getCoach().getCoachNumber());
        ticket.setSeatNo(booking.getCoach().getSeatNo());
        ticket.setName(booking.getPassenger().getName());
        ticket.setUsername(booking.getPassenger().getUsername());
        ticket.setStatus(booking.getStatus());
        ticket.setBerth(booking.getCoach().getBerth());
        ticket.setDate(booking.getDate());
        return ticket;
    }
}
