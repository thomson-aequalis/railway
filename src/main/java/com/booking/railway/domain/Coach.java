package com.booking.railway.domain;

import com.booking.railway.enums.Berth;
import com.booking.railway.enums.CoachNumber;
import com.booking.railway.enums.CoachStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Entity
@Table(name = "coach")
public class Coach {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "seat_no", nullable = false)
    private Integer seatNo;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "coach_number", nullable = false)
    private CoachNumber coachNumber;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "berth", nullable = false)
    private Berth berth;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status", nullable = false)
    private CoachStatus status;
}
