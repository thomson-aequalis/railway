package com.booking.railway.domain.dto;

import com.booking.railway.enums.Berth;
import com.booking.railway.enums.BookingStatus;
import com.booking.railway.enums.CoachNumber;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Ticket {
    private String cnfNo;
    private String username;
    private String name;
    private CoachNumber coachNumber;
    private Integer seatNo;
    private BookingStatus status;
    private Berth berth;
    private Date date;
}
