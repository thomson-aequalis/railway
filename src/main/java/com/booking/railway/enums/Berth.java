package com.booking.railway.enums;

public enum Berth {
    LOWER,
    UPPER,
    MIDDLE,
    SIDE_LOWER,
    SIDE_UPPER;
}
