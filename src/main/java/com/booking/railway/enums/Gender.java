package com.booking.railway.enums;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
