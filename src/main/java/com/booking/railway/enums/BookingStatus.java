package com.booking.railway.enums;

public enum BookingStatus {
    CONFIRMED,
    WAITING_LIST,
    CANCELLED;
}
