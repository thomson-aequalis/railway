package com.booking.railway.enums;

public enum CoachStatus {
    BOOKED,
    OPEN
}
