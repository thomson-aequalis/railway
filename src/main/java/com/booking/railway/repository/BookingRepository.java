package com.booking.railway.repository;

import com.booking.railway.domain.Booking;
import com.booking.railway.enums.Berth;
import com.booking.railway.enums.BookingStatus;
import com.booking.railway.enums.CoachNumber;
import com.booking.railway.enums.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface BookingRepository extends JpaRepository<Booking, Long> {

    int countByStatus(BookingStatus status);

    List<Booking> findByCoachBerth(Berth berth);

    @Query("SELECT b.passenger.gender FROM Booking b WHERE b.coach.coachNumber=?1")
    Set<Gender> findByCoachCoachNumber(CoachNumber coachNumber);
}
