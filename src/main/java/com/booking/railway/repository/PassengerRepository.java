package com.booking.railway.repository;

import com.booking.railway.domain.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    boolean existsByUsername(String username);
}
