package com.booking.railway.repository;

import com.booking.railway.domain.Coach;
import com.booking.railway.enums.Berth;
import com.booking.railway.enums.CoachNumber;
import com.booking.railway.enums.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface CoachRepository extends JpaRepository<Coach, Long> {

    @Query("SELECT (count(c)=0) FROM Coach c")
    boolean isCoachEmpty();

    @Query("SELECT c.seatNo FROM Coach c WHERE c.coachNumber=?1")
    List<Integer> findBookedSeatsByCoachNumber(CoachNumber coachNumber);

    boolean existsByCoachNumberAndBerthAndSeatNo(CoachNumber coachNumber, Berth berth, Integer seatNo);

}
