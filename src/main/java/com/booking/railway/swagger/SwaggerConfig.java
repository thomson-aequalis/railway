package com.booking.railway.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Thomson on 04/06/19
 * @version 1
 * @copyright Copyright (c) 2019 The Land Administration Company Inc. All rights reserved.
 * @project LAVA Master
 * @descrpition
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {

        return new Docket(DocumentationType.SWAGGER_12)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.booking.railway"))
            .paths(PathSelectors.any())
            .build();
    }
}
